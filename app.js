var budgetController = (function () {
    var Expense = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    }
    var Income = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    }
    var calculateTotal = function(type){
        sum=0;
        data.allItems[type].forEach(function(element){
            sum+= element.value;
        });
        data.totals[type] = sum;
    }
    var data = {
        allItems: {
            exp: [],
            inc: []
        },
        totals: {
            exp: 0,
            inc: 0
        },
        budget: 0
    }
    return {
        addItem: function (type, des, value) {
            var newItem, ID, reqArray;
            reqArray = data.allItems[type];
            if (reqArray.length > 1) {
                ID = reqArray[reqArray.length - 1].id + 1;
            }
            else {
                ID = 0;
            }

            if (type === 'exp') {
                newItem = new Expense(ID, des, value);
            }
            else if (type === 'inc') {
                newItem = new Income(ID, des, value);
            }
            reqArray.push(newItem);
            return newItem;
        },
        calculateBudget: function() {
            var totalInc,totalExp;
            calculateTotal('exp');
            calculateTotal('inc');
            [totalInc , totalExp] = [data.totals.inc,data.totals.exp];            
            data.budget = totalInc - totalExp;
            if(data.totals.inc >0){
                data.percentage = Math.round((totalExp / totalInc) * 100)  
            }
            else{
                data.percentage = -1;
            }
        },
        getBudget: function(){
            return {
                budget: data.budget,
                totalInc: data.totals.inc,
                totalExp: data.totals.exp,
                percentage: data.percentage
            };
        },
        testing: function () {
            console.log(data);
        }
    }
})();

var UIController = (function () {
    var DOMstrings = {
        inputType: '.add__type',
        inputDescription: '.add__description',
        inputValue: '.add__value',
        inputBtn: '.add__btn',
        incomeContainer: '.income__list',
        expenseContainer: '.expenses__list',
        budgetLabel: '.budget__value',
        incomeLabel:'.budget__income--value',
        expensesLabel:'.budget__expenses--value',
        percentageLabel:'.budget__expenses--percentage',
        container: '.container'

    }

    return {
        getInput: function () {
            return {
                type: document.querySelector(DOMstrings.inputType).value,
                description: document.querySelector(DOMstrings.inputDescription).value,
                value: parseFloat(document.querySelector(DOMstrings.inputValue).value)
            }
        },
        addListItem: function (obj, type) {
            var html,selector;
            if (type === 'inc') {
                html = `<div class="item clearfix" id="inc-${obj.id}"><div class="item__description">${obj.description}</div><div class="right clearfix"><div class="item__value">+ ${obj.value}</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>`;
                selector = DOMstrings.incomeContainer;
            }
            else if (type === 'exp') {
                html = `<div class="item clearfix" id="exp-${obj.id}"><div class="item__description">${obj.description}</div><div class="right clearfix"><div class="item__value">- ${obj.value}</div><div class="item__percentage">21%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>`;
                selector = DOMstrings.expenseContainer;
            }
            document.querySelector(selector).insertAdjacentHTML('beforeend',html);
        },
        clearFields: function () {
            var fields,fieldsArr;
            fields = document.querySelectorAll(DOMstrings.inputDescription+ ' ,' +DOMstrings.inputValue);
            fieldsArr = Array.prototype.slice.call(fields);
            fieldsArr.forEach(function(current){
                current.value = "";
            });
            fieldsArr[0].focus();
        },
        displayBudget: function(obj){
            document.querySelector(DOMstrings.budgetLabel).textContent = obj.budget;
            document.querySelector(DOMstrings.incomeLabel).textContent = obj.totalInc;
            document.querySelector(DOMstrings.expensesLabel).textContent = obj.totalExp;
            if(obj.percentage >0){
                document.querySelector(DOMstrings.percentageLabel).textContent = obj.percentage + 
                '%';
            }
            else{
                document.querySelector(DOMstrings.percentageLabel).textContent = '---'; 
            }
        },
        getDOMstrings: function () {
            return DOMstrings;
        }
    }

})();

var controller = (function (budgetCtrl, UICtrl) {
    setupEventListeners = function () {
        const DOM = UICtrl.getDOMstrings();
        document.querySelector(DOM.inputBtn).addEventListener('click', ctrlAddItem);
        document.addEventListener('keypress', function (e) {
            if (e.keyCode === 13 || e.which === 13) {
                ctrlAddItem();
            }
        });
        document.querySelector(DOM.container).addEventListener('click',ctrlDeleteItem);
    };

    const updateBudget = function(){
        var budget;
        budgetCtrl.calculateBudget();
        budget = budgetCtrl.getBudget();
        UICtrl.displayBudget(budget); 
    }

    const ctrlAddItem = function () {
        var input, newItem;
        input = UICtrl.getInput();
        if(input.value != 0 && !isNaN(input.value) && input.description != ""){
            newItem = budgetController.addItem(input.type, input.description, input.value);
            UIController.addListItem(newItem,input.type);
            UIController.clearFields();
            updateBudget();
        }
    };
    const ctrlDeleteItem = function(event){
        var itemID,splitID,type,ID;
        itemID = event.target.parentNode.parentNode.parentNode.parentNode.id;
        if(itemID.indexOf('inc') >=0 || itemID.indexOf('exp') >= 0){
            splitID = itemID.split('-');
            type = splitID[0];
            ID = splitID[1];
        }
    }
    return {
        init: function () {
            console.log('Application has started');
            setupEventListeners();
            UICtrl.displayBudget({
                budget: 0,
                totalInc: 0,
                totalExp: 0,
                percentage: 0
            });
        }
    }

})(budgetController, UIController);

controller.init();